//
//  OnboardingSecondPageVC.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 17.01.2021.
//

import UIKit



class OnboardingSecondPageVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var titileLabel: UILabel!
    
    @IBOutlet weak var labelFirst: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    
    @IBOutlet weak var labelSecond: UILabel!
    @IBOutlet weak var labelSecondName: UILabel!
    
    @IBOutlet weak var labelThird: UILabel!
    @IBOutlet weak var labelThirdName: UILabel!
    
    @IBOutlet weak var labelFourth: UILabel!
    @IBOutlet weak var labelFourhtName: UILabel!
    
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Properties
    
//    var presenter: OnboardingPresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        presenter = OnboardingPresenter(view: self)
        
        setupViews()
        
        // background

        
        backgroundView.backgroundColor = .appColor(.skipButtonBackground)
        imageBackground.image = .appImage(.onboardigPage2)
        imageBackground.alpha = 0.25

        // Title label
       
        
        titileLabel.text = "FD"
        titileLabel.textColor = .appColor(.appWhite)
        titileLabel.font = .appFont(name: .bold, size: 40)

        // First pair of labels
        labelFirst.text = "30+"
        labelFirst.textColor = .appColor(.appWhite)
        labelFirst.font = .appFont(name: .bold, size: 30)
        
        labelFirstName.text = "Photo Studios"
        labelFirstName.textColor = .appColor(.appWhite)
        labelFirstName.font = .appFont(name: .regular, size: 14)
        
       

        // Second pair of labels
        labelSecond.text = "64000"
        labelSecond.textColor = .appColor(.appWhite)
        labelSecond.font = .appFont(name: .bold, size: 30)
        
        labelSecondName.text = "Sq.ft. of Space"
        labelSecondName.textColor = .appColor(.appWhite)
        labelSecondName.font = .appFont(name: .regular, size: 14)
        
        
        // Third pair of labels
        labelThird.text = "20 153"
        labelThird.textColor = .appColor(.appWhite)
        labelThird.font = .appFont(name: .bold, size: 30)
        
        labelThirdName.text = "Happy Customers"
        labelThirdName.textColor = .appColor(.appWhite)
        labelThirdName.font = .appFont(name: .regular, size: 14)
//
        
        // Fourth pair of labels
        labelFourth.text = "75 321"
        labelFourth.textColor = .appColor(.appWhite)
        labelFourth.font = .appFont(name: .bold, size: 30)
        
            
        labelFourhtName.text = "Succesful Photos"
        labelFourhtName.textColor = .appColor(.appWhite)
        labelFourhtName.font = .appFont(name: .regular, size: 14)
        
        
        // skip button
        skipButton.backgroundColor = .appColor(.skipButtonBackground)
        skipButton.layer.cornerRadius = skipButton.layer.frame.height/2
        skipButton.setTitleColor(.appColor(.appWhite), for: .normal)
        skipButton.setTitle("Skip", for: .normal)
        
        
        // page control
//        let pageVC = OnboardingPageVC()
        pageControl.currentPage = 1
        pageControl.numberOfPages = 2
      
        
        
        
        
    }
    
    // MARK: - UI
    
    private func setupViews() {
        
    }
    
    // MARK: - IBActions
    
}

extension OnboardingSecondPageVC: OnboardingViewControllerProtocol {
    

    
}
