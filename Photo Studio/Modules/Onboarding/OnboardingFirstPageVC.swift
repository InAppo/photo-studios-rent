//
//  OnboardingFirstPageVC.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 17.01.2021.
//

import UIKit



protocol OnboardingViewControllerProtocol: BaseViewControllerProtocol {
    
}

class OnboardingFirstPageVC: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var titileLabel: UILabel!
    
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelSecond: UILabel!
    
    @IBOutlet weak var skipButton: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    // MARK: - Properties
    
    //    var presenter: OnboardingPresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        presenter = OnboardingPresenter(view: self)
        
        // background
        
        let colorTop =  UIColor(red: 0.055, green: 0.059, blue: 0.067, alpha: 1).cgColor
        let colorBottom = UIColor(red: 0.275, green: 0.29, blue: 0.302, alpha: 1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = backgroundView.bounds
        gradientLayer.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradientLayer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 0, c: 0, d: 0.21, tx: 0, ty: 0.29))
        gradientLayer.bounds = backgroundView.bounds.insetBy(dx: -1*backgroundView.bounds.size.width,
                                                             dy: -2*backgroundView.bounds.size.height)
        gradientLayer.position = backgroundView.center
        
        backgroundView.layer.insertSublayer(gradientLayer, at:0)
        
        
        
        
        imageBackground.image = .appImage(.onboardigPage1)
        imageBackground.alpha = 0.25
        
        // Title label
        titileLabel.text = "FD"
        titileLabel.textColor = .appColor(.appWhite)
        titileLabel.font = .appFont(name: .bold, size: 40)
        
        // First label
        labelOne.text = "Rental Photo Studio \nNew York & Los Angeles"
        labelOne.textColor = .appColor(.appWhite)
        labelOne.font = .appFont(name: .bold, size: 27)
        
        // Second Label
        labelSecond.text = "Every month we roundup the freshest new web sites that have been released in the previous four weeks, with an eye-out for new ideas."
        labelSecond.textColor = .appColor(.appWhite)
        labelSecond.font = .appFont(name: .regular, size: 14)
        
        // skip button
        skipButton.backgroundColor = .appColor(.skipButtonBackground)
        skipButton.layer.cornerRadius = skipButton.layer.frame.height/2
        skipButton.setTitleColor(.appColor(.appWhite), for: .normal)
        skipButton.setTitle("Skip", for: .normal)
        
        
        // page control
        
        pageControl.currentPage = 0
        pageControl.numberOfPages = 2
        
        setupViews()
    }
    
    // MARK: - UI
    
    private func setupViews() {
        
    }
    
    // MARK: - IBActions
    
}

extension OnboardingFirstPageVC: OnboardingViewControllerProtocol {
    
    
    
}
