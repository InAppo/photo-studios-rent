//
//  OnboardingPageVC.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 17.01.2021.
//

import UIKit

class OnboardingPageVC: UIPageViewController{
    
    var currentPage = 0
    var maximumPages = 1
    

   lazy var arrayOfVC : [UIViewController] = {
        let sb = UIStoryboard(name: "Onboarding", bundle: nil)
        
        let test1 = sb.instantiateViewController(withIdentifier: "OnboardingFirstPageVC") as! OnboardingFirstPageVC
        let test2 = sb.instantiateViewController(withIdentifier: "OnboardingSecondPageVC") as! OnboardingSecondPageVC

        return [test1, test2]

    
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.dataSource = self

        
        setViewControllers([arrayOfVC[0]], direction: .forward, animated: true, completion: nil)
        
    }
    
    
}

extension OnboardingPageVC: UIPageViewControllerDataSource {
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return arrayOfVC.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = arrayOfVC.firstIndex(of: viewController)!
    
        if (currentIndex <= 0) { return nil }
        return arrayOfVC[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex: Int = arrayOfVC.firstIndex(of: viewController)!
       
        if (currentIndex >= arrayOfVC.count - 1) { return nil }
        return arrayOfVC[currentIndex + 1]
        
    }
    
    
    
}
