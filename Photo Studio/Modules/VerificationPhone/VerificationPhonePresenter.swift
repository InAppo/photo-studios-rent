//
//  VerificationPhonePresenter.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 30.11.2020.
//

import Foundation

protocol VerificationPhonePresenterProtocol {
    init(view: VerificationPhoneViewControllerProtocol, id: Int, phone: String)
    func pressShowLogin()
}

class VerificationPhonePresenter: VerificationPhonePresenterProtocol {
    
    private unowned let view: VerificationPhoneViewControllerProtocol

    required init(view: VerificationPhoneViewControllerProtocol, id: Int, phone: String) {
        self.view = view
        self.clientId = id
        self.phone = phone
    }
    
    // MARK: - Properties
    private let clientId: Int
    private let phone: String
    
    // MARK: - Actions
    func pressShowLogin() {
        let vc = LoginViewController.instance(.authorization)
        vc.presenter = LoginPresenter(view: vc, isLogin: true)
        self.view.pushToVC(vc, animated: true)
    }
    
}
