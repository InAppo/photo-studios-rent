//
//  VerificationPhoneViewController.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 30.11.2020.
//

import UIKit

protocol VerificationPhoneViewControllerProtocol: BaseViewControllerProtocol {
    
}

class VerificationPhoneViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var bottomTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var placeholderPhoneLabel: UILabel!
    @IBOutlet weak var centerTextView: UITextView!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thridTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    // MARK: - Properties
    
    var presenter: VerificationPhonePresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    // MARK: - UI
    
    private func setupViews() {
        self.view.setGradientBackground()
        confirmButton.setGradientButton()
        setupTextFields()
        setupPlaceholderLabel()
        setupTextViews()
    }
    
    private func setupTextViews() {
        centerTextView.hyperLink(originalText: "Didn’t recieve code? Request again", hyperLink: ["Request again"], urlString: ["RequestAgain"], styleAligment: .center)
        bottomTextView.hyperLink(originalText: "Already have an account? Sign In", hyperLink: ["Sign In"], urlString: ["SignIn"], styleAligment: .center)
        centerTextView.delegate = self
        bottomTextView.delegate = self
    }
    
    private func setupPlaceholderLabel() {
        let attrs1 = [NSAttributedString.Key.font : UIFont.appFont(familyFont: .beVietnam, name: .regular, size: 14), NSAttributedString.Key.foregroundColor : UIColor.appColor(.textColor)]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.appFont(familyFont: .beVietnam, name: .regular, size: 14), NSAttributedString.Key.foregroundColor : UIColor.appColor(.blueApp)]
        
        let attributedString1 = NSMutableAttributedString(string:"Code is sent to ", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:"+3 (123) 456 78 90", attributes:attrs2)
        
        attributedString1.append(attributedString2)
        self.placeholderPhoneLabel.attributedText = attributedString1
    }
    
    private func setupTextFields() {
        firstTextField.viewBorder(color: .appColor(.borderText), width: 1)
        secondTextField.viewBorder(color: .appColor(.borderText), width: 1)
        thridTextField.viewBorder(color: .appColor(.borderText), width: 1)
        fourthTextField.viewBorder(color: .appColor(.borderText), width: 1)
        firstTextField.keyboardType = .numberPad
        secondTextField.keyboardType = .numberPad
        thridTextField.keyboardType = .numberPad
        fourthTextField.keyboardType = .numberPad
        firstTextField.delegate = self
        secondTextField.delegate = self
        thridTextField.delegate = self
        fourthTextField.delegate = self
    }
    
    // MARK: - IBActions
    @IBAction func pressConfirmButton(_ sender: UIButton) {
        
    }
    
    @IBAction func enterOne(_ sender: UITextField) {
        if sender.text?.count ?? 0 == 0 {
            beackStep(textField: sender)
        } else {
            nextStep(textField: sender)
        }
    }
    
    private func nextStep(textField: UITextField) {
        switch textField {
        case firstTextField: secondTextField.becomeFirstResponder()
        case secondTextField: thridTextField.becomeFirstResponder()
        case thridTextField: fourthTextField.becomeFirstResponder()
        case fourthTextField: fourthTextField.resignFirstResponder()
        default:
            break
        }
    }
    
    private func beackStep(textField: UITextField) {
        switch textField {
        case firstTextField: break
        case secondTextField: firstTextField.becomeFirstResponder()
        case thridTextField: secondTextField.becomeFirstResponder()
        case fourthTextField: thridTextField.becomeFirstResponder()
        default:
            break
        }
    }
    
}

extension VerificationPhoneViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.viewBorder(color: .appColor(.blueApp), width: 1)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 < 1 {
            textField.viewBorder(color: .appColor(.borderText), width: 1)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text?.count ?? 0 < 1 || range.length == 1 {
            return true
        } else {
            return false
        }
    }
    
}

extension VerificationPhoneViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == "SignIn" {
            presenter.pressShowLogin()
        } else if URL.absoluteString == "RequestAgain" {
            print("RequestAgain")
        }
        return false
    }
    
}

extension VerificationPhoneViewController: VerificationPhoneViewControllerProtocol {
    
}
