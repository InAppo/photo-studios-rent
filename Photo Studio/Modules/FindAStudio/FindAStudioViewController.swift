//
//  FindAStudioViewController.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 31.01.2021.
//

import UIKit

protocol FindAStudioViewControllerProtocol: BaseViewControllerProtocol {
    func reloadCollection()
    func setMonth(_ month: String)
    func setStartTime(_ time: String?)
    func setFinishTime(_ time: String?)
}

class FindAStudioViewController: UIViewController {
        
    // MARK: - IBOutlets
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var startAtButton: UIButton!
    @IBOutlet weak var startAtLabel: UILabel!
    @IBOutlet weak var finishAtButton: UIButton!
    @IBOutlet weak var finishAtLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    
    // MARK: - Properties
    
    var presenter: FindAStudioPresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = FindAStudioPresenter(view: self)
        
        setupViews()
    }
    
    // MARK: - UI
    
    private func setupViews() {
        self.view.setGradientBackground()
        setupCollectionView()
        searchButton.setGradientButton()
    }
    
    private func setupCollectionView() {
        collectionView.register(DateCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    // MARK: - IBActions
    @IBAction func pressCityButton(_ sender: UIButton) {
    }
    
    @IBAction func pressLeftMonthButton(_ sender: UIButton) {
        presenter.pressLeftButton()
    }
    
    @IBAction func pressRightMonthButton(_ sender: UIButton) {
        presenter.pressRightButton()
    }
    
    @IBAction func pressStartAtButton(_ sender: UIButton) {
        presenter.pressStartAtButton()
    }
    
    @IBAction func pressFinishAtButton(_ sender: UIButton) {
        presenter.pressFinishAtButton()
    }
    
    @IBAction func pressSearchButton(_ sender: UIButton) {
    }
    
    @IBAction func pressAllStudios(_ sender: UIButton) {
    }
    
}

extension FindAStudioViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.getCountItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.create(DateCollectionViewCell.self, indexPath)
        presenter.configurateCell(cell, index: indexPath.row)
        return cell
    }

}

extension FindAStudioViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.pressCell(indexPath.row)
    }
    
}

extension FindAStudioViewController: FindAStudioViewControllerProtocol {
    
    func reloadCollection() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func setMonth(_ month: String) {
        monthLabel.text = month
    }
    
    func setStartTime(_ time: String?) {
        if let time = time {
            startAtLabel.text = "Start at"
            startAtButton.setTitle(time, for: .normal)
        } else {
            startAtLabel.text = ""
            startAtButton.setTitle("Start at", for: .normal)
        }
    }
    
    func setFinishTime(_ time: String?) {
        if let time = time {
            finishAtLabel.text = "Finish at"
            finishAtButton.setTitle(time, for: .normal)
        } else {
            finishAtLabel.text = ""
            finishAtButton.setTitle("Finish at", for: .normal)
        }
    }
    
}
