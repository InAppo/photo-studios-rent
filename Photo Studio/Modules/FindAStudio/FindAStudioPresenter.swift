//
//  FindAStudioPresenter.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 31.01.2021.
//

import Foundation

protocol FindAStudioPresenterProtocol {
    init(view: FindAStudioViewControllerProtocol)
    func configurateCell(_ cell: DateCollectionViewCellProtocol, index: Int)
    func getCountItems() -> Int
    func pressCell(_ index: Int)
    func pressLeftButton()
    func pressRightButton()
    func pressStartAtButton()
    func pressFinishAtButton()
}

class FindAStudioPresenter: FindAStudioPresenterProtocol {
    
    private unowned let view: FindAStudioViewControllerProtocol

    required init(view: FindAStudioViewControllerProtocol) {
        self.view = view
        let formatMonth = DateFormatter()
        formatMonth.dateFormat = "MMMM"
        self.currentMonth = formatMonth.string(from: Date())
        self.view.setMonth(self.currentMonth)
        
        let months = Calendar.current.monthSymbols
        print(months)
        
        let format = DateFormatter()
        format.dateFormat = "MMMM dd yyyy HH:mm"
        
        var dictionary = [String : [Date]]()
        
        for month in months {
            let dateMonth = format.date(from: "\(month) 01 2021 12:00")!
            dictionary[month] = dateMonth.getAllDays()
        }
      
        print(dictionary)
        
        self.dictionaryMonth = dictionary
    }
    
    // MARK: - Properties
    private var dictionaryMonth: [String : [Date]]
    private var currentMonth: String
    private var selectedItem = [String : Date]()
    
    // MARK: - UI
    func configurateCell(_ cell: DateCollectionViewCellProtocol, index: Int) {
        let formatDayWeak = DateFormatter()
        formatDayWeak.dateFormat = "EE"
        let formatDay = DateFormatter()
        formatDay.dateFormat = "dd"
        let date = dictionaryMonth[currentMonth]![index]
        cell.display(date: formatDay.string(from: date))
        cell.display(day: formatDayWeak.string(from: date))
        cell.display(isSelect: selectedItem[currentMonth] == date)
    }
    
    func getCountItems() -> Int {
        return dictionaryMonth[currentMonth]?.count ?? 0
    }
    
    // MARK: - Actions
    func pressCell(_ index: Int) {
        self.selectedItem = [currentMonth : dictionaryMonth[currentMonth]![index]]
        self.view.reloadCollection()
    }
    
    func pressLeftButton() {
        if currentMonth != "January" {
            let months = Calendar.current.monthSymbols
            let currentIndex = months.firstIndex(of: currentMonth)!
            currentMonth = months[currentIndex - 1]
            self.view.setMonth(currentMonth)
            self.view.reloadCollection()
        }
    }
    
    func pressRightButton() {
        if currentMonth != "December" {
            let months = Calendar.current.monthSymbols
            let currentIndex = months.firstIndex(of: currentMonth)!
            currentMonth = months[currentIndex + 1]
            self.view.setMonth(currentMonth)
            self.view.reloadCollection()
        }
    }
    
    func pressStartAtButton() {
        let vc = StartAtTimeViewController.instance(.filter)
        vc.presenter = StartAtTimePresenter(view: vc, callback: {[weak self] (time) in
            guard let self = self else { return }

            self.view.setStartTime(time)
        })
        
        self.view.pushToVC(vc, animated: true)
    }
    
    func pressFinishAtButton() {
        let vc = StartAtTimeViewController.instance(.filter)
        vc.presenter = StartAtTimePresenter(view: vc, callback: {[weak self] (time) in
            guard let self = self else { return }

            self.view.setFinishTime(time)
        })
        
        self.view.pushToVC(vc, animated: true)
    }
    
    // MARK: - Buisnes Logic
    private func createItems() -> [String : [Date]] {
        let months = Calendar.current.monthSymbols
        print(months)
        
        let format = DateFormatter()
        format.dateFormat = "MMMM dd yyyy HH:mm"
        
        var dictionary = [String : [Date]]()
        
        for month in months {
            let dateMonth = format.date(from: "\(month) 01 2021 12:00")!
            dictionary[month] = dateMonth.getAllDays()
        }
      
        print(dictionary)
        
        return dictionary
    }
    
}
