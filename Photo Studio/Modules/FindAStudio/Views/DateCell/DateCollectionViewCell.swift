//
//  DateCollectionViewCell.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 31.01.2021.
//

import UIKit

protocol DateCollectionViewCellProtocol {
    func display(date: String?)
    func display(day: String?)
    func display(isSelect: Bool)
}

class DateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        resetContent()
        setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        resetContent()
    }
     
    private func resetContent() {
        dateLabel.text = nil
        dayLabel.text = nil
    }
    
    private func setupView() {
        containerView.viewCorner(25)
    }
    
}

extension DateCollectionViewCell: DateCollectionViewCellProtocol {
    
    func display(date: String?) {
        dateLabel.text = date
    }
    
    func display(day: String?) {
        dayLabel.text = day
    }
    
    func display(isSelect: Bool) {
        if isSelect {
            self.containerView.viewBorder(color: .appColor(.blueApp), width: 1)
            self.containerView.backgroundColor = .black
        } else {
            self.containerView.viewBorder(color: .clear, width: 0)
            self.containerView.backgroundColor = .appColor(.confirmButton)
        }
    }
    
}
