//
//  LoginPresenter.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 29.11.2020.
//

import UIKit

protocol LoginPresenterProtocol {
    init(view: LoginViewControllerProtocol, isLogin: Bool)
    func configurateCell(_ cell: TextFieldTableViewCellProtocol, item: Int)
    func getCountItems() -> Int
    func pressLogin(isLogin: Bool)
    func pressConfirm()
    func onLoadView()
}

class LoginPresenter: LoginPresenterProtocol {
    
    private unowned let view: LoginViewControllerProtocol

    required init(view: LoginViewControllerProtocol, isLogin: Bool) {
        self.view = view
        self.isLogin = isLogin
    }
    
    // MARK: - Properties
    private var isLogin: Bool
    private var dataSignUp = [SignUpItems : String]()
    private var dataSignIn = [SignInItems : String]()
    private let authApi = AuthApi()
    
    // MARK: - UI
    func onLoadView() {
        self.view.setupUI(isLogin: isLogin)
    }
    
    func configurateCell(_ cell: TextFieldTableViewCellProtocol, item: Int) {
        if isLogin {
            guard let type = SignInItems.init(rawValue: item) else { return }
            
            cell.display(isPasword: type.isPasword)
            cell.display(title: type.title)
            cell.display(keybordType: type.keybordType)
            cell.display(textContent: type.contentType)
            cell.display(text: dataSignIn[type])
            var cell = cell
            cell.textHandler = { [weak self] text in
                guard let self = self else { return }
                
                self.dataSignIn[type] = text
            }
        } else {
            guard let type = SignUpItems.init(rawValue: item) else { return }
            
            cell.display(isPasword: type.isPasword)
            cell.display(title: type.title)
            cell.display(keybordType: type.keybordType)
            cell.display(textContent: type.contentType)
            cell.display(text: dataSignUp[type])
            var cell = cell
            cell.textHandler = { [weak self] text in
                guard let self = self else { return }
                
                self.dataSignUp[type] = text
            }
        }
    }
    
    func getCountItems() -> Int {
        return isLogin ? SignInItems.allCases.count : SignUpItems.allCases.count
    }
    
    // MARK: - Actions
    func pressLogin(isLogin: Bool) {
        self.isLogin = isLogin
        self.view.setupUI(isLogin: self.isLogin)
    }
    
    func pressConfirm() {
        if isLogin {
            self.login()
        } else {
            self.register()
        }
    }
    
    // MARK: - API
    private func login() {
        guard let login = dataSignIn[.email], let password = dataSignIn[.password] else { return }
        
        authApi.login(login: login, password: password) {[weak self] (data, error) in
            guard let self = self else { return }

            if let error = error {
                print(error)
                return
            }
            
            if let data = data as? AuthResponse {
                print(data)
                UserDefaultsManager().token = data.accessToken
                UserDefaultsManager().refreshToken = data.refreshToken
            }
        }
    }
    
    private func register() {
        guard let name = dataSignUp[.fullName], let email = dataSignUp[.email],
              let password = dataSignUp[.password], let phone = dataSignUp[.phone]
        else { return }
        
        authApi.register(name: name, email: email, phone: phone) {[weak self] (data, error) in
            guard let self = self else { return }

            if let error = error {
                print(error)
                return
            }
            
            if let data = data as? SignupResponse {
                print(data)
                let vc = VerificationPhoneViewController.instance(.authorization)
                vc.presenter = VerificationPhonePresenter(view: vc, id: data.clientId, phone: phone)
                self.view.pushToVC(vc, animated: true)
            }
        }
    }
    
}
