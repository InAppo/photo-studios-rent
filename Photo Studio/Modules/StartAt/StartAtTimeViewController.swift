//
//  StartAtTimeViewController.swift
//  TestPhotoStodio
//
//  Created by Коля Мамчур on 02.12.2020.
//

import UIKit

protocol StartAtTimeViewControllerProtocol: BaseViewControllerProtocol {
    
}

class StartAtTimeViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var hoursPicker: UIPickerView!
    
    @IBOutlet weak var minutePicker: UIPickerView!
    
    @IBOutlet weak var amPmPicker: UIPickerView!
    @IBOutlet weak var buttonConfirm: UIButton!
    
    @IBOutlet weak var confirmImage: UIImageView!
    @IBOutlet weak var constrainLefttoButtonText: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var lineNeedBackground: UIView!
    
    
    
    // MARK: - Properties
    
    var presenter: StartAtTimePresenterProtocol!
    
    var  minuteArray : [String] = []
    var hoursArray : [String] = []
    let amPmArray : [String] = ["AM", "PM"]
    
    
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        presenter = StartAtTimePresenter(view: self)
        
        setupViews()
        
        
        
        // title label
        titleLabel.text = "Start At"
        
        
        let colorTop =  UIColor(red: 0, green: 0.467, blue: 1, alpha: 1).cgColor
        let colorBottom = UIColor(red: 0, green: 0.745, blue: 1, alpha: 1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = lineNeedBackground.bounds
        
        gradientLayer.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        lineNeedBackground.layer.insertSublayer(gradientLayer, at: 0)
        
        
        hoursPicker.delegate = self
        hoursPicker.dataSource = self
        
        hoursPicker.setValue(UIColor.white, forKey: "textColor")
        
        
        
        
        
        
        minutePicker.delegate = self
        minutePicker.dataSource = self
        
        minutePicker.setValue(UIColor.white, forKey: "textColor")
        
        amPmPicker.delegate = self
        amPmPicker.dataSource = self
        
        amPmPicker.setValue(UIColor.white, forKey: "textColor")
        
        
        
        hoursPicker.subviews.first?.subviews.last?.backgroundColor = UIColor.white
        
        buttonConfirm.backgroundColor = .appColor(.confirmButton)
        buttonConfirm.setTitle("Confirm", for: .normal)
        
        confirmImage.image = .appImage(.confirmImage)
        
        for i in 0...59 {
            minuteArray.append("\(i)")
        }
        for i in 1...12 {
            hoursArray.append("\(i)")
        }
        
        
        
    }
    
    deinit {
        print("!!!!!!!!!!!!!!!!!!")
        print("deinit")
    }
    
    // MARK: - UI
    
    private func setupViews() {
        self.view.setGradientBackground()
    }
    
    // MARK: - IBActions
    @IBAction func pressCloseButton(_ sender: UIButton) {
        self.popVC(animated: true)
    }
    
    @IBAction func pressConfirmButton(_ sender: UIButton) {
        presenter.pressConfirmButton()
    }
    
}

extension StartAtTimeViewController:  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView == minutePicker) {
            return 60
            
        }else if (pickerView == hoursPicker){
            return 12
        }else {
            return 2
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        for lineSubview in pickerView.subviews {
            if lineSubview.frame.height < 100 {
                lineSubview.backgroundColor = UIColor.white.withAlphaComponent(0)
            }
        }
        
        if (pickerView == minutePicker) {
            
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont.appFont(name: .bold, size: 20)
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = minuteArray[row].count < 2 ? "0\(minuteArray[row])" : minuteArray[row]
            pickerLabel?.textColor = .white
            
            return pickerLabel!
            
        }
        
        else if (pickerView == hoursPicker){
            
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont.appFont(name: .bold, size: 20)
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = hoursArray[row].count < 2 ? "0\(hoursArray[row])" : hoursArray[row]
            pickerLabel?.textColor = .white
            
            return pickerLabel!
            
            
        } else {
            var pickerLabel: UILabel? = (view as? UILabel)
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont.appFont(name: .bold, size: 20)
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = amPmArray[row]
            pickerLabel?.textColor = .white
            
            return pickerLabel!
            
            
        }
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        
        
        
        return 50
    }
    
    
    
    
}

extension StartAtTimeViewController: StartAtTimeViewControllerProtocol {
    
    
    
}
