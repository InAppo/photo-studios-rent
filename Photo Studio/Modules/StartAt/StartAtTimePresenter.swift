//
//  StartAtTimePresenter.swift
//  TestPhotoStodio
//
//  Created by Коля Мамчур on 02.12.2020.
//

import Foundation

protocol StartAtTimePresenterProtocol {
    init(view: StartAtTimeViewControllerProtocol, callback: @escaping ((String) -> ()))
    func pressConfirmButton()
}

class StartAtTimePresenter: StartAtTimePresenterProtocol {
    
    private unowned let view: StartAtTimeViewControllerProtocol

    required init(view: StartAtTimeViewControllerProtocol, callback: @escaping ((String) -> ())) {
        self.view = view
        self.callback = callback
    }
    
    // MARK: - Properties
    private let callback: ((String) -> ())
    
    // MARK: - Actions
    func pressConfirmButton() {
        self.callback("12:30 pm")
        self.view.popVC(animated: true)
    }
    
}
