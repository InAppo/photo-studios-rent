//
//  MainScreenPresenter.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 06.12.2020.
//

import Foundation

protocol MainScreenPresenterProtocol {
    init(view: MainScreenViewControllerProtocol)
}

class MainScreenPresenter: MainScreenPresenterProtocol {
    
    private unowned let view: MainScreenViewControllerProtocol

    required init(view: MainScreenViewControllerProtocol) {
        self.view = view
    }
    
}
