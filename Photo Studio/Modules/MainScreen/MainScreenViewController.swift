//
//  MainScreenViewController.swift
//  Photo Studio
//
//  Created by Дмитро Мостовий on 06.12.2020.
//

import UIKit

protocol MainScreenViewControllerProtocol: BaseViewControllerProtocol {

}

class MainScreenViewController: UIViewController {
        
    // MARK: - IBOutlets
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var presenter: MainScreenPresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MainScreenPresenter(view: self)
        
        setupViews()
    }
    
    // MARK: - UI
    
    private func setupViews() {
        self.view.setGradientBackground()
        
        setupTabBar()
        setupTableView()
    }
    
    private func setupTabBar() {
        let tabBar: TabBarView = .fromNib()
        tabBarView.addSubview(tabBar)
        tabBarView.constrainToEdges(tabBar)
    }
    
    private func setupTableView() {
        tableView.register(FhotoStudioTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK: - IBActions
    
}

extension MainScreenViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.create(FhotoStudioTableViewCell.self, indexPath)
        return cell
    }
    
}

extension MainScreenViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = .clear
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
}

extension MainScreenViewController: MainScreenViewControllerProtocol {

}
